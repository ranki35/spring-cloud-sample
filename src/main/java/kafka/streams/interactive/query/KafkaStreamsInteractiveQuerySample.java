/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kafka.streams.interactive.query;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;

import kafka.streams.interactive.query.avro.Song;
import kafka.streams.interactive.query.avro.SongPlayCount;

@SpringBootApplication
public class KafkaStreamsInteractiveQuerySample {

	public static void main(String[] args) {
		SpringApplication.run(KafkaStreamsInteractiveQuerySample.class, args);
	}

	@EnableBinding(KStreamProcessorX.class)
	public static class KStreamMusicSampleApplication {

		@StreamListener
		@SendTo("output")
		public KStream<String, SongPlayCount> process(@Input("input") KStream<Long, Song> songs) {
			return songs.map((key, value) -> new KeyValue<>("2", SongPlayCount.newBuilder().setSongId(2L).build()));
		}
	}

	interface KStreamProcessorX {

		@Input("input")
		KStream<?, ?> input();

		@Output("output")
		KStream<?, ?> output();
	}
}
